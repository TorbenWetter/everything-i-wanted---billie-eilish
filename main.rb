define :base_part1 do
  play :cs5
  play :fs4
  play :d3
  sleep 0.6
  play :a4
  sleep 0.6
  play :b4
  play :gs4
  play :e3
  sleep 2
  play :cs5
  play :e4
  play :cs3
  sleep 0.6
  play :gs4
  sleep 0.6
  play :cs5
  play :a4
  play :d3
  sleep 1.2
  play :a4
  sleep 0.2
  play :b4
  sleep 0.2
  play :cs5
  sleep 0.2
  play :d5
  sleep 0.2
end

define :base_part1b do
  play :cs5
  play :fs4
  play :d3
  sleep 0.6
  play :a4
  sleep 0.6
  play :b4
  play :gs4
  play :e3
  sleep 2
  play :cs5
  play :b4
  play :es4
  play :cs3
  sleep 0.6
  play :gs4
  sleep 0.6
  play :cs5
  play :a4
  play :d3
  sleep 1.2
  play :a4
  sleep 0.2
  play :b4
  sleep 0.2
  play :cs5
  sleep 0.2
  play :d5
  sleep 0.2
end

define :base_part2 do
  play :cs5
  play :fs4
  play :d3
  sleep 0.6
  play :a4
  sleep 0.4
  play :gs4
  sleep 0.2
  play :b4
  play :e3
  sleep 2
  play :gs4
  play :cs3
  sleep 0.6
  play :cs5
  play :e4
  sleep 0.6
  play :a4
  play :fs4
  play :d3
  sleep 2
end

define :base_part2b do
  play :cs5
  play :fs4
  play :d3
  sleep 0.6
  play :a4
  sleep 0.4
  play :gs4
  sleep 0.2
  play :b4
  play :e3
  sleep 2
  play :gs4
  play :cs3
  sleep 0.6
  play :cs5
  play :e4
  sleep 0.6
  play :a4
  play :fs4
  play :d3
  sleep 0.4
  2.times do
    play :d3
    sleep 0.2
    play :fs3
    sleep 0.2
    play :a3
    sleep 0.2
    play :cs4
    sleep 0.2
  end
end

define :base_part3 do
  2.times do
    play :d3
    sleep 0.2
    play :fs3
    sleep 0.2
    play :a3
    sleep 0.2
    play :cs4
    sleep 0.2
  end
  2.times do
    play :e3
    sleep 0.2
    play :gs3
    sleep 0.2
    play :b3
    sleep 0.2
    play :e4
    sleep 0.2
  end
  2.times do
    play :cs3
    sleep 0.2
    play :e3
    sleep 0.2
    play :gs3
    sleep 0.2
    play :cs4
    sleep 0.2
  end
  2.times do
    play :d3
    sleep 0.2
    play :fs3
    sleep 0.2
    play :a3
    sleep 0.2
    play :cs4
    sleep 0.2
  end
end

define :base_part3b do
  2.times do
    play :d3
    sleep 0.2
    play :fs3
    sleep 0.2
    play :a3
    sleep 0.2
    play :cs4
    sleep 0.2
  end
  2.times do
    play :e3
    sleep 0.2
    play :gs3
    sleep 0.2
    play :b3
    sleep 0.2
    play :e4
    sleep 0.2
  end
  2.times do
    play :cs3
    sleep 0.2
    play :e3
    sleep 0.2
    play :gs3
    sleep 0.2
    play :cs4
    sleep 0.2
  end
  play :d3
  sleep 0.2
  play :fs3
  sleep 0.2
  play :a3
  sleep 0.2
  play :cs4
  sleep 0.2
  sleep 0.8
end

define :voice_part1 do
  sleep 0.8
  sleep 0.2
  play :cs5
  sleep 0.2
  play :b4
  sleep 0.2
  play :a4
  sleep 0.2
  play :gs4
  sleep 0.8
  sleep 0.8
  sleep 0.4
  play :gs4
  sleep 0.2
  play :gs4
  sleep 0.2
  play :e5
  sleep 0.2
  play :d5
  sleep 0.2
  play :cs5
  sleep 0.2
  play :d5
  sleep 0.2
  play :d5
  sleep 0.4
  play :d5
  play :cs5
  sleep 0.4
  sleep 0.8
end

define :voice_part2 do
  sleep 0.8
  sleep 0.2
  play :cs5
  sleep 0.2
  play :b4
  sleep 0.2
  play :a4
  sleep 0.2
  play :gs4
  sleep 0.8
  sleep 0.8
  sleep 0.4
  sleep 0.2
  play :gs4
  sleep 0.2
  play :e5
  sleep 0.2
  play :d5
  sleep 0.2
  play :cs5
  sleep 0.2
  play :d5
  sleep 0.2
  play :d5
  sleep 0.4
  play :cs5
  sleep 0.2
  play :b4
  sleep 0.2
  play :cs5
  sleep 0.2
  play :b4
  sleep 0.2
  play :a4
  sleep 0.2
  play :gs4
  sleep 0.2
  play :a4
  sleep 0.4
  play :fs4
  sleep 0.4
  sleep 0.8
  sleep 0.4
  sleep 0.2
  play :fs4
  sleep 0.2
  play :cs5
  sleep 0.2
  play :b4
  sleep 0.2
  play :a4
  sleep 0.2
  play :gs4
  sleep 0.2
  play :gs4
  sleep 0.4
  play :e4
  sleep 0.4
  sleep 0.8
  sleep 1.6
end

define :voice_part3 do
  sleep 0.8
  sleep 0.2
  play :cs5
  sleep 0.2
  play :b4
  sleep 0.2
  play :a4
  sleep 0.2
  play :gs4
  sleep 0.8
  sleep 0.8
  sleep 0.4
  play :gs4
  sleep 0.2
  play :gs4
  sleep 0.2
  play :e5
  sleep 0.2
  play :d5
  sleep 0.2
  play :cs5
  sleep 0.2
  play :d5
  sleep 0.2
  play :d5
  sleep 0.4
  play :cs5
  sleep 0.4
  sleep 0.4
  sleep 0.2
  play :cs5
  sleep 0.2
  play :cs5
  sleep 0.6
  play :e5
  sleep 0.2
  play :d5
  sleep 0.4
  play :cs5
  sleep 0.2
  play :b4
  sleep 0.4
  play :gs4
  sleep 0.2
  sleep 0.4
  sleep 0.8
  play :gs4
  sleep 0.4
  sleep 0.2
  play :cs5
  sleep 0.6
  play :fs4
  sleep 0.4
  sleep 0.8
  sleep 0.2
  play :a4
  sleep 0.2
  play :b4
  sleep 0.2
  play :cs5
  sleep 0.2
end

define :voice_part4 do
  sleep 0.4
  sleep 0.2
  play :e5
  sleep 0.2
  play :fs5
  sleep 0.4
  play :fs5
  sleep 0.2
  play :gs5
  sleep 0.8
  play :e5
  sleep 1.4
  play :e5
  sleep 0.2
  play :fs5
  sleep 0.2
  play :e5
  sleep 0.4
  play :d5
  sleep 0.2
  play :d5
  sleep 0.4
  play :cs5
  sleep 0.2
  sleep 0.4
  sleep 0.8
  sleep 0.4
  play :e5
  sleep 0.2
  play :fs5
  sleep 0.2
  play :fs5
  sleep 0.4
  play :fs5
  sleep 0.2
  play :gs5
  sleep 0.8
  play :e5
  sleep 1.4
  play :d5
  sleep 0.2
  play :e5
  sleep 0.2
  play :e5
  sleep 0.4
  play :d5
  sleep 0.2
  play :d5
  sleep 0.4
  play :cs5
  sleep 0.2
  sleep 0.4
  sleep 0.8
end

define :voice_part5 do
  sleep 0.4
  play :fs5
  sleep 0.4
  play :fs5
  sleep 0.4
  play :a5
  sleep 0.2
  play :fs5
  sleep 0.2
  play :gs5
  sleep 1.2
  play :e5
  sleep 0.2
  play :b5
  sleep 1
  play :a5
  sleep 0.2
  play :gs5
  sleep 0.4
  play :gs5
  sleep 0.4
  play :a5
  sleep 0.2
  play :fs5
  sleep 0.4
  sleep 0.8
  sleep 0.2
  play :e5
  sleep 0.2
  play :e5
  sleep 0.2
  play :fs5
  sleep 0.2
  play :fs5
  sleep 0.4
  play :fs5
  sleep 0.2
  play :gs5
  sleep 0.6
  play :e5
  sleep 0.2
  play :e5
  sleep 1.4
  play :cs5
  sleep 0.4
  play :e5
  sleep 0.4
  play :b4
  sleep 0.2
  play :d5
  sleep 0.4
  play :cs5
  sleep 0.2
  sleep 0.4
  sleep 0.8
end

# TODO: add function with parameters (?) to play a sound [more than one per line?] and wait
# TODO: split base functions into base and tenor (save lines with specific combinations)

use_bpm 48

live_loop :base do
  use_synth :piano
  with_fx :reverb, room: 1 do
    with_fx :ixi_techno do
      base_part1
      base_part2
      2.times do
        base_part1
        base_part2
        base_part1
      end
      2.times do
        base_part1
        base_part2
      end
      base_part1b
      base_part2b
    end
    base_part3
    base_part3b
  end
end

live_loop :voice do
  use_synth :pluck
  8.times do
    sleep 1.6
  end
  2.times do
    voice_part1
    voice_part2
  end
  voice_part3
  voice_part4
  voice_part5
  8.times do
    sleep 1.6  # TODO: add voice parts
  end
end